#include <stdlib.h>

typedef struct {
    T version;
    T blkCt;
    T info;
    T name;
    T protoList;
    T supportJRTime;
    T inst;
} ump_endpoint_t;

void connectDevice();
void umpEndpointDiscovery(ump_endpoint_t *ep);
void selectProtocol();
void getFnBlocks();
void detectMidiCI();

int main() {
    ump_endpoint_t *ep = (ump_endpoint_t *)malloc(1 * sizeof(ump_endpoint_t));

    connectDevice();
    umpEndpointDiscovery(ep);
    selectProtocol();
    getFnBlocks();
    detectMidiCI();

    for(;;) {
        if (1/*TODO*/)  { break; }

        // TODO: Use MIDI-CI

        // TODO: Use MIDI
    }

    return EXIT_SUCCESS;
}

void connectDevice() {

}

void umpEndpointDiscovery(ump_endpoint_t *ep) {
    ep->version = 0;
    ep->blkCt = 0;
    ep->info = 0;
    ep->name = 0;
    ep->protoList = 0;
    ep->supportJRTime = 0;
    ep->inst = 0;
}

void selectProtocol() {

}

void getFnBlocks() {

}

void detectMidiCI() {

}

