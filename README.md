# midi2.0

## Checklist

- [ ] Get/set via JSON
- [ ] Protocol handshake
- [ ] Message setup
- [ ] Subscribe / notify ?
- [ ] Backwards compatibility
- [ ] UMP

_Process: use MIDI-CI to test for 2.0_

## Notes

* _A channel voice message will be similar to 1.0 except mt=4 (packet start) and **index** comes after **status**_
* **Note On**: (64 bits) mt=4 (4) group (4) status (4) chan (4) x (1) note (7) attribute type e.g. 0x03 for "precise pitch" (8) velocity (7) x (9) attribute (7 + 9)
* JR = jitter reduction
* UMP = universal MIDI packet (16 groups of 16 channels)
* Utility: _Midi2Scope_ (currently a giant clusterfuck developed internally by members of MIDI Assoc)
    
|  mt  | desc   |
|------|--------|
|  0   | utility (32b) |
|  1   | sys RT and common msg (32) |
|  2   | midi 1.0 chan vox msg (32) |
|  3   | data msg (incl. sysex) (64) |
|  4   | midi 2.0 chan vox msg (64) |
|  5   | data msg (128) |
|  6+  | _rsvd_ |

## TODO: Description
TODO

# (TODO: Badges)

# (TODO: Visuals)

## TODO: Installation
TODO

## TODO: Usage
TODO

## Support
This is just an exploration of the protocol. This will change later if I get traction.

## TODO: Roadmap
TODO

## TODO: Contributing
TODO

## TODO: Authors and acknowledgment
TODO

## License
MIT License

TODO: insert license text

## TODO: Project status
TODO

## Criticisms
_some of these are possibly invalid or inaccurate_

* Too much resolution for some parameters. Simply, nobody needs to have 4 billion pitch divisions. The range of human hearing can be split into 5 uHz divisions. For technical reasons, it would make sense for instrumentation to allow such divisions but probably if the split was logarithmic or in a way that equally divides sound energy.
* Missed opportunity to reduce traffic by creating an incremental stream. For instance, Stream A is assigned to pitch bend. Every x ms, send a full description of pitch bend. For the "in-between", A:+1, get an update several us later, A:+2, us, A:+1, us, A:+1, us, A:-1, and so on. For such a stream, you'd only need a few bits, and the transmissions can be asynchronous (publisher-listener) I imagine MPE is somehow implementing this.
* Missed opportunity to make latency reduction even better. Set Stream B to "note on" for a keyboard with 112 notes (88 + 24 for an extra octave) and use the remaining 16 bits of a 128-bit transmission for ECC. This transmits repeatedly until the listener ACKs, and the listener should understand that this is only one command for "note on".
* The above could be best implemented with frame-based communication e.g. CAN FD. In fact, I'm not sure why MIDI doesn't have a basis in any of the modern protocols (ROS, CAN, Ethernet) or with an eye toward the future e.g. web, async, many-controlled-by-few or few-controlled-by-many
* MIDI 2.0 should have a lot of pressure to adopt USB-C (or have a great reason to prefer an alternative and choose an alternative) as there are seemingly few advantages to the 5-pin DIN plug other than existing players having them.
* Too much support for system exclusive messages. If a microcontroller was 80% custom instructions, it would be total chaos for the support ecosystem (design tools, debug tools, software, peripherals, etc).
* From what I've heard, there isn't good native support for instruments with alternative positions. Think about this... On a guitar, you can play the same note on 3 or 4 different strings&mdash;each with its own specific vibrational and timbre characteristics. Same with any stringed instrument. Same with trombone and most brass instruments. You can strike a marimba key directly in the middle or near the side or in the corner, and this excites the key in a different way than just "velocity and pitch". Cymbals (and most other percussion instruments) take this to the extreme. Even reed instruments can change sound based on embouchure and the airstream of the player. The standard MIDI packet should support this in a strongly opinionated manner rather than leaving it to the will of each device manufacturer.
* Leave some room for reserved functionality. Open up this functionality regularly to suggestions. See Unicode, emojis, the ABC standard, and Arm trace protocols.
