/* system exclusive sub ID #1 = 0x0D */
static const PROFILE_INQUIRY = 0x20;
static const REPLY_TO_PROFILE_INQUIRY = 0x21;
static const PROFILE_ADDED_REPORT = 0x26;
static const PROFILE_REMOVED_REPORT = 0x27;
static const SET_PROFILE_ON = 0x22;
static const SET_PROFILE_OFF = 0x23;
static const PROFILE_ENABLED_REPORT = 0x24;
static const PROFILE_DISABLED_REPORT = 0x25;
static const PROFILE_DETAILS_INQUIRY_MESSAGE = 0x28;
static const REPLY_TO_PROFILE_DETAILS_INQUIRY_MESSAGE = 0x29;

/* mode messages, CC #120--127 */
static const ALL_SOUND_OFF = 120;
static const RESET_ALL_CONTROLLERS = 121;
static const LOCAL_ON_OFF = 122;
static const ALL_NOTES_OFF = 123;
static const OMNI_MODE_OFF = 124;
static const OMNI_MODE_ON = 125;
static const MONO_MODE_ON = 126;
static const POLY_MODE_ON = 127;

typedef struct {
    uint8_t start;
    uint8_t excl;
    uint8_t id;
    uint8_t sxs1;
    uint8_t sxs2;
    uint8_t ver;
    uint32_t src;
    uint32_t dst;
    uint16_t sz;
    uint8_t *data;
    uint8_t end;
} midi_ci_msg_t;

uint8_t dev;
uint8_t s2;
uint8_t v;
uint32_t sMuid;
uint32_t dMuid;

dev = 0x7F;  /* to/from fn block */
s2 = 0x70;  /* discovery */
dMuid = 0x7F7F7F7F;  /* to Broadcast MUID */
uint8_t data[17] = {1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1};

midi_ci_msg_t message = {0xF0, 0x7E, dev, 0x0D, s2, v, sMuid, dMuid, data, 0xF7};
